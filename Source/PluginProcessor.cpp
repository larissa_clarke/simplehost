/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic startup code for a Juce application.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
SimpleHostAudioProcessor::SimpleHostAudioProcessor()
{
    formatManager.addDefaultFormats();
}

SimpleHostAudioProcessor::~SimpleHostAudioProcessor()
{
}

//==============================================================================
const String SimpleHostAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

int SimpleHostAudioProcessor::getNumParameters()
{
    return 0;
}

float SimpleHostAudioProcessor::getParameter (int index)
{
    return 0.0f;
}

void SimpleHostAudioProcessor::setParameter (int index, float newValue)
{
}

const String SimpleHostAudioProcessor::getParameterName (int index)
{
    return String::empty;
}

const String SimpleHostAudioProcessor::getParameterText (int index)
{
    return String::empty;
}

const String SimpleHostAudioProcessor::getInputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

const String SimpleHostAudioProcessor::getOutputChannelName (int channelIndex) const
{
    return String (channelIndex + 1);
}

bool SimpleHostAudioProcessor::isInputChannelStereoPair (int index) const
{
    return true;
}

bool SimpleHostAudioProcessor::isOutputChannelStereoPair (int index) const
{
    return true;
}

bool SimpleHostAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool SimpleHostAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool SimpleHostAudioProcessor::silenceInProducesSilenceOut() const
{
    return false;
}

double SimpleHostAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int SimpleHostAudioProcessor::getNumPrograms()
{
    return 0;
}

int SimpleHostAudioProcessor::getCurrentProgram()
{
    return 0;
}

void SimpleHostAudioProcessor::setCurrentProgram (int index)
{
}

const String SimpleHostAudioProcessor::getProgramName (int index)
{
    return String::empty;
}

void SimpleHostAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void SimpleHostAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    String errorMessage;
	PluginDescription desc;
	desc.pluginFormatName = "VST";
	desc.fileOrIdentifier = "/Users/larissa/Library/Audio/Plug-Ins/VST/TestPlug.vst";
    
    AudioPluginInstance* instance = formatManager.createPluginInstance (desc, sampleRate, samplesPerBlock, errorMessage);
    
    AudioProcessorGraph::Node* node = nullptr;
    
    if (instance != nullptr)
    {
        node = graph.addNode (instance);
    }
    
    if (node != nullptr)
    {
        node->properties.set ("x", 0);
        node->properties.set ("y", 0);
        
    }
    else
    {
        DBG("Couldn't create filter");
        DBG(errorMessage);
        
    }

}

void SimpleHostAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

void SimpleHostAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    for (int channel = 0; channel < getNumInputChannels(); ++channel)
    {
        //float* channelData = buffer.getWritePointer (channel);

        // ..do something to the data...
    }

    // In case we have more outputs than inputs, we'll clear any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    for (int i = getNumInputChannels(); i < getNumOutputChannels(); ++i)
    {
        buffer.clear (i, 0, buffer.getNumSamples());
    }
}

//==============================================================================
bool SimpleHostAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* SimpleHostAudioProcessor::createEditor()
{
    return new SimpleHostAudioProcessorEditor (this);
}

//==============================================================================
void SimpleHostAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void SimpleHostAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new SimpleHostAudioProcessor();
}
