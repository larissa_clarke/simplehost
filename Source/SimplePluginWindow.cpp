#include "SimplePluginWindow.h"

SimplePluginWindow::SimplePluginWindow (Component* const uiComp,
                                        AudioProcessorGraph::Node* owner_,
                                        const bool isGeneric_)
: DocumentWindow (uiComp->getName(), Colours::lightblue,
                  DocumentWindow::minimiseButton | DocumentWindow::closeButton),
owner (owner_),
isGeneric (isGeneric_)
{
    setUsingNativeTitleBar(true);
    
    setSize (400, 300);
    
    setContentOwned (uiComp, true);
    
    setTopLeftPosition (owner->properties.getWithDefault ("uiLastX", Random::getSystemRandom().nextInt (500)),
                      owner->properties.getWithDefault ("uiLastY", Random::getSystemRandom().nextInt (500)));
    setVisible (true);
    
    setAlwaysOnTop(true);
    
}

SimplePluginWindow::~SimplePluginWindow()
{
    DBG("Deleting simple plugin window...");
    //clearContentComponent();
}


void SimplePluginWindow::moved()
{
    owner->properties.set ("uiLastX", getX());
    owner->properties.set ("uiLastY", getY());
}


void SimplePluginWindow::closeButtonPressed()
{
    DBG("SimplePluginWindow::closeButtonPressed");
    //setVisible(false);
    delete this;
}

SimplePluginWindow* SimplePluginWindow::getWindowFor (AudioProcessorGraph::Node* node,
                                                      bool useGenericView)
{
    
    AudioProcessorEditor* ui = nullptr;
    
    if (! useGenericView)
    {
        ui = node->getProcessor()->createEditorIfNeeded();
        
        if (ui == nullptr)
            useGenericView = true;
    }
    
    if (useGenericView)
        ui = new GenericAudioProcessorEditor (node->getProcessor());
    
    if (ui != nullptr)
    {
        AudioPluginInstance* const plugin = dynamic_cast <AudioPluginInstance*> (node->getProcessor());
        
        if (plugin != nullptr)
            ui->setName (plugin->getName());
        
        return new SimplePluginWindow (ui, node, useGenericView);
    }
    
    return nullptr;
}
